<?php
	error_reporting(0);
	$url = $_GET["v"];
	$videoKod = $url;

	//Provjerava ako postoji vise od jedne rijeci u upitu, ako je istinito salje podatke na search.php fajl
	if(substr_count($url, ' ') > 0){
		header("Location: http://localhost/Brorix/search.php?q=$url");
		die();
	}else if (strlen($url) < 11 || strlen($url) > 11){
		header("Location: http://localhost/Brorix/search.php?q=$url");
		die();
	}

	//Provjerava ako je $url prazan da vraca na pocetnu stranicu
	if ($url == ""){
		header("Location: http://localhost/Brorix/");
		die();
	}

	//Uredjuje link koji je korisnik unio na pocetnoj stranici te tako dobiva ID videa, uzima ikonu slike i postavlja url preko cega se poziva video u html-u
	$noviURL = "http://www.youtube.com/v/$videoKod?version=3&loop=1&playlist=$videoKod&autoplay=1&showinfo=0";
	$slika = "http://img.youtube.com/vi/$videoKod/1.jpg";
	//Dobiva URL sa informacijama videa
	$typeID = 1230;
	$url = "https://gdata.youtube.com/feeds/api/videos/$videoKod?v=2&typeid=$typeID&regionlimit=10000002";

	//Uzima kod sa stranice sa informacijama videa i sprema ga u varijablu $videoInfo
	$sxml = simplexml_load_file($url);
	ob_start();
	var_dump($sxml);
	$videoInfo = ob_get_clean();

	//Algoritam pomocu cega se sprema ime videa u varijablu $videoTitle
	$mjestoTrazenja = strpos($videoInfo, "[\"title\"]");
	$duljinaNaslova = substr($videoInfo, ($mjestoTrazenja + 21), 2);
	$videoTitle = substr($videoInfo, ($mjestoTrazenja + 21 + 5), $duljinaNaslova);

	/*
	//Algoritam koji ce pronaci web stranicu i uzeti kod te stranice sa koje ce kasnije preuzeti popis pjesama tog izvodjaca
	$MjestoPovlake = strpos($videoTitle, "-");
	$ImeIzvodjaca = substr($videoTitle, 0, $MjestoPovlake);
	$ImeIzvodjacaInfo = file_get_contents("http://search.azlyrics.com/search.php?q=$ImeIzvodjaca");
	$ImeIzvodjacaNew = str_replace(" ", "", $ImeIzvodjaca);
	$ImeIzvodjacaNew = strtolower($ImeIzvodjacaNew);
	$duljinaIzvodjaca = strlen($ImeIzvodjacaNew);
	$mjestoLinkaIzvodjaca = substr($ImeIzvodjacaInfo, strpos($ImeIzvodjacaInfo, "1. <a href=") + 12, 31 + $duljinaIzvodjaca);

	if ($ImeIzvodjacaNew == "ellahenderson"){
		$mjestoLinkaIzvodjaca = "http://www.azlyrics.com/e/ellahenderson.html";
	}*/

	//Izvlaci imena pjesama sa web stranice
	/*$LinkPopisPjesama = file_get_contents($mjestoLinkaIzvodjaca);
	$LinkPopisPjesama = substr($LinkPopisPjesama, 1, strpos($LinkPopisPjesama, "other songs:") - 19);
	$mjesto = strpos($LinkPopisPjesama, 'target="_blank">');
	$ImePjesme[1] = substr($LinkPopisPjesama, $mjesto + 16, strpos($LinkPopisPjesama, "</a><br />", $mjesto) - ($mjesto + 16));

	$SvePjesme = $ImePjesme[1] . "<br>";

	for ($i = 1; $i <= 99; $i++){
		$mjesto = strpos($LinkPopisPjesama, 'target="_blank">', $mjesto + 16);
		$ImePjesme[$i] = substr($LinkPopisPjesama, $mjesto + 16, strpos($LinkPopisPjesama, "</a><br />", $mjesto) - ($mjesto + 16));
		
		if (strpos($ImePjesme[$i], "-//W3C//DTD XHTML 1.0 Transitional") == TRUE){
			$ImePjesme[$i] = "";
			$i = 99;
			$ImePjesme[$i] = str_replace("[", " [", $ImePjesme[$i]);
		}else{

			for ($ponovi = 1; $ponovi < $i ; $ponovi++) { 
				if ($ImePjesme[$i] == $ImePjesme[$i - $ponovi]){
					$ImePjesme[$i] = "";
				}
			}
			$ImePjesme[$i] = str_replace("[", " [", $ImePjesme[$i]);
			$SvePjesme = $SvePjesme . $ImePjesme[$i] . "<br>";
			//$SvePjesme = substr($SvePjesme, 0, -4);
		}

		if ($ImePjesme[$i] == ""){
			$brojPjesama = $i;
			$SvePjesme = substr($SvePjesme, 0, -4);
			$i = 99;
		}
	}*/

	//Iz imena pjesme izbacuje znakove zbog kojih se ne moze pronaci pjesma u bazi podataka za azlyrics.com
	$videoTitleNew = strtolower($videoTitle);
	$videoTitleNew = str_replace("-", " ", $videoTitleNew);
	$videoTitleNew = str_replace("(", "", $videoTitleNew);
	$videoTitleNew = str_replace(")", "", $videoTitleNew);
	$videoTitleNew = str_replace("official", "", $videoTitleNew);
	$videoTitleNew = str_replace("music", "", $videoTitleNew);
	$videoTitleNew = str_replace("video", "", $videoTitleNew);
	$videoTitleNew = str_replace("edited", "", $videoTitleNew);
	$videoTitleNew = str_replace("/", " ", $videoTitleNew);
	$videoTitleNew = str_replace("original", "", $videoTitleNew);
	$videoTitleNew = str_replace("extended", "", $videoTitleNew);
	$videoTitleNew = str_replace("version", "", $videoTitleNew);
	$videoTitleNew = str_replace("feat", "", $videoTitleNew);
	$videoTitleNew = str_replace("high quality", "", $videoTitleNew);
	$videoTitleNew = str_replace("best quality", "", $videoTitleNew);
	$videoTitleNew = str_replace("lyrics", "", $videoTitleNew);
	$videoTitleNew = str_replace("ft.", "", $videoTitleNew);
	$videoTitleNew = str_replace(".", "", $videoTitleNew);
	$videoTitleNew = str_replace("hd", "", $videoTitleNew);
	$videoTitleNew = str_replace("hq", "", $videoTitleNew);
	$videoTitleNew = str_replace(",", "", $videoTitleNew);
	$videoTitleNew = str_replace("[", "", $videoTitleNew);
	$videoTitleNew = str_replace("]", "", $videoTitleNew);
	$videoTitleNew = str_replace("{", "", $videoTitleNew);
	$videoTitleNew = str_replace("}", "", $videoTitleNew);
	$videoTitleNew = str_replace(":", "", $videoTitleNew);
	$videoTitleNew = str_replace("!", "", $videoTitleNew);
	$videoTitleNew = str_replace("&", "", $videoTitleNew);
	$videoTitleNew = str_replace('"', "", $videoTitleNew);
	$videoTitleNew = str_replace("out now", "", $videoTitleNew);
	$videoTitleNew = str_replace("1975", "", $videoTitleNew);

	$copyVideoTitle = $videoTitleNew;
	if (strpos($copyVideoTitle, "50") == TRUE || strpos($copyVideoTitle, "cent") == TRUE && strpos($copyVideoTitle, "my") == TRUE && strpos($copyVideoTitle, "life") == TRUE){
		$videoTitleNew = "50 cent my life";
	}
	if (strpos($copyVideoTitle, "gilbert") == TRUE || strpos($copyVideoTitle, "o'sullivan") == TRUE && strpos($copyVideoTitle, "alone") == TRUE && strpos($copyVideoTitle, "again") == TRUE) {
		$videoTitleNew = "neil diamond alone again naturally";
	}
	if (strpos($copyVideoTitle, "the") == TRUE && strpos($copyVideoTitle, "fault") == TRUE && strpos($copyVideoTitle, "in") == TRUE && strpos($copyVideoTitle, "our") == TRUE && strpos($copyVideoTitle, "stars") == TRUE && strpos($copyVideoTitle, "boom") == TRUE && strpos($copyVideoTitle, "clap") == TRUE) {
		$videoTitleNew = "charli xcx boom clap";
	}
	if (strpos($copyVideoTitle, "charlie") == TRUE || strpos($copyVideoTitle, "xcx") == TRUE && strpos($copyVideoTitle, "boom") == TRUE && strpos($copyVideoTitle, "clap") == TRUE) {
		$videoTitleNew = "charli xcx boom clap";
	}
	if (strpos($copyVideoTitle, "skrillex") == TRUE || strpos($copyVideoTitle, "bangarang") == TRUE && strpos($copyVideoTitle, "sirah") == TRUE) {
		$videoTitleNew = "skrillex bangarang";
	}

	//Nakon sto pretrazi tekstove uzima prvi lyric i sprema link web stranice
	$kodPretrazivanjaLyricsStranice = file_get_contents("http://search.azlyrics.com/search.php?q=$videoTitleNew");
	$mjestoLinkaLyricsa = strpos($kodPretrazivanjaLyricsStranice, "1. <a href=\"");
	$krajLinkaLyricsa = strpos($kodPretrazivanjaLyricsStranice, "\">", $mjestoLinkaLyricsa);
	$linkLyricsStranice = substr($kodPretrazivanjaLyricsStranice, ($mjestoLinkaLyricsa + 12), ($krajLinkaLyricsa - ($mjestoLinkaLyricsa + 12)));

	//Sprema tekst sa stranice u varijablu $lyrics
	$kodLyricsStranice = file_get_contents("$linkLyricsStranice");
	$pocetakLyricsa = strpos($kodLyricsStranice, "<!-- start of lyrics -->");
	$krajLyricsa = strpos($kodLyricsStranice, "<!-- end of lyrics -->");
	$lyrics = substr($kodLyricsStranice, ($pocetakLyricsa + 24), ($krajLyricsa - $pocetakLyricsa + 24));
	$lyrics = str_replace("<!-- end of lyrics -->", "", $lyrics);
	$lyrics = "<br>" . $lyrics;

	if ($lyrics == "" || $lyrics == "<br>"){
		$lyrics = "<br>Sorry, we couldn't find the lyrics for this song. <br> -BRORIX Team";
	}

?>
<html>
	<head>
		<title>BRORIX <?php echo "$videoTitle"; ?></title>
		<meta charset="UTF-8">
		<!-- <script src="http://code.jquery.com/jquery-latest.min.js"></script> -->
		<link rel="shortcut icon" href="./img/brorix_icon.png">
		<style type="text/css">
			#header{
				left: 0px;
				top: 0px;
				position: fixed;
				width: 100%;
				height: 75px;
				background-color: #33CC33;
				font-size: 20px;
				padding-top: 20px;
				padding-left: 2px;
				margin: 0px;
				z-index: 100;
			}
			#logoImg{
				position: fixed;
				top: 8px;
				left: 100px;
			}
			#searchBox{
				border-radius: 10px;
				width: 275px;
				height: 30px;
				font-size: 15px;
				position: fixed;
				left: 50%;
				margin-left: -155px;
				top: 30px;
			}
			#searchIcon{
				position: fixed;
				border-radius: 5px;
				top: 30px;
				left: 50%;
				margin-left: 125px;
			}
			div{
				text-align: center;
				position: relative;
			}
			body{
				background-color: #009933;
				font-family: Arial;
				color: white;
				text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
			}
			#title{
				font-size: 25px;
				font-weight: bold;
				margin-bottom: 8px;
				position: relative;
			}
			#lyrics{
				text-align: center;
			}
			div.hide{ 
				display: none;
			}
			.ShowLyrics{
				color: black;
			}
			button{
				border: none;
				height: 80px;
				width: 80px;
				position: absolute;
				font-size: 18px;
			}
			.ShowHideLyrics{
				background-color: yellow;
				top: 0px;
			}
			.invisible{
				opacity: 0;
			}
			.YoutubeLink{
				background-color: red;
				background-image: url("http://icons.iconarchive.com/icons/dakirby309/windows-8-metro/256/Web-Youtube-alt-2-Metro-icon.png");
				background-size: 80px 80px;
				top: 80px;
			}
			.FacebookLink{
				background-color: blue;
				background-image: url("https://shop.freiheit.org/images/icons/facebook.jpg");
				background-size: 80px 80px;
				top: 160px;
			}
			.TwitterLink{
				background-color: lightblue;
				background-image: url("http://icons.iconarchive.com/icons/danleech/simple/1024/twitter-icon.png");
				background-size: 80px 80px;
				top: 240px;
			}
			#FavoriteLink{
				background-color: orange;
				background-image: url("http://brorix.comxa.com/img/favorite_icon.png");
				background-size: 80px 80px;
				top: 320px;
			}
			.grey{
				color: lightgrey;
				font-size: 10px;
				text-shadow: none;
				right: 10px;
				bottom: 10px;
			}
			#FavoriteList{
				position: fixed;
				left: 20px;
				color: orange;
				font-size: 20px;
			}
			#allFavorites{
				position: fixed;
				left: 20px;
				color: blue;
				top: 180px;
				z-index: 999;
			}
			a.anchor{
    			text-decoration: none;
    			color: blue;
    		}
		</style>
	</head>

	<body onresize="resizedWindow()">
		<h1 id="header">
			<img id="logoImg" src="./img/BRORIX_logo.png" width="180" height="75">
			<form action="search.php" method="GET">
				<input type="text" id="searchBox" align="middle" name="q" placeholder=" Search for a song...">
				<input type="image" src="./img/search_icon.png" alt="Submit Form" width="30" height="30" id="searchIcon">
			</form>
		</h1>

		<br><br><br><br><br>

		
		<p id="title"><?php echo "$videoTitle"; ?></p>
		<p id="FavoriteList">Favorites:</p>
		<p id="allFavorites" style="text-decoration:none"></p>

		<div>
			<iframe class="videoFrame" id="player1" onload="floaded1()" width="720" height="400" src="<?= $noviURL ?>" frameborder="0" allowfullscreen></iframe>
			<button class="ShowHideLyrics" onclick="showLyrics('text1','text2')"><a href="javascript:void(0);" style="text-decoration:none" class="ShowLyrics" id="show_hide_lyrics">Show Lyrics</a></button>
			<button class="YoutubeLink"><a onclick="window.open('http://www.youtube.com/watch?v=<?php echo $videoKod ?>', 'width=450, height=500'); return false;" style="text-decoration:none" class="invisible">YOUTUBE<br>YOUTUBE<br>YOUTUBE</a></button>
			<button class="FacebookLink"><a onclick="window.open(FacebookShareLinkJavascript, 'newwindow', 'width=450, height=500'); return false;" style="text-decoration:none" class="invisible">FACEBOOK<br>FACEBOOK<br>FACEBOOK</a></button>
			<button class="TwitterLink"><a onclick="window.open('https://twitter.com/share', 'newwindow', 'width=450, height=500'); return false;" style="text-decoration:none" class="invisible">TWITTER<br>TWITTER<br>TWITTER</a></button>
			<button id="FavoriteLink"><a onclick="add_remove_Favorite()" style="text-decoration:none" class="invisible">FAVORITE<br>FAVORITE<br>FAVORITE</a></button>
		</div>
		<div id="text1" class="hide"> <?php echo "$lyrics"; ?> </div>

		<script type="text/javascript"> 

			/* Funkcija kojom pretazujem sve kolacice po imenu i izbacuje vrijednost kolacica */
			function readCookie(name){
				var nameEQ = name + "=";
				var ca = document.cookie.split(';');
				for(var i = 0; i < ca.length; i++) {
					var c = ca[i];
					while (c.charAt(0)==' ') c = c.substring(1,c.length);
					if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
				}
				return null;
			}

			// Funkcija koja cisti uneseni string tako da je kompatibilan sa azlyrics searchom. Uglavnom, samo brise crte, zareze, tocke, ...
			function convertForSearch(originalSongName){
				originalSongName = originalSongName.toLowerCase();
				editedSongName = originalSongName.replace("-", " ");
				editedSongName = editedSongName.replace("(", "");
				editedSongName = editedSongName.replace(")", "");
				editedSongName = editedSongName.replace("official", "");
				editedSongName = editedSongName.replace("music", "");
				editedSongName = editedSongName.replace("video", "");
				editedSongName = editedSongName.replace("edited", "");
				editedSongName = editedSongName.replace("soundtrack", "");
				editedSongName = editedSongName.replace("/", "");
				editedSongName = editedSongName.replace("original", "");
				editedSongName = editedSongName.replace("extended", "");
				editedSongName = editedSongName.replace("version", "");
				editedSongName = editedSongName.replace("feat", "");
				editedSongName = editedSongName.replace("high quality", "");
				editedSongName = editedSongName.replace("best quality", "");
				editedSongName = editedSongName.replace("lyrics", "");
				editedSongName = editedSongName.replace("ft.", "");
				editedSongName = editedSongName.replace(".", "");
				editedSongName = editedSongName.replace("hd", "");
				editedSongName = editedSongName.replace("hq", "");
				editedSongName = editedSongName.replace(",", "");
				editedSongName = editedSongName.replace("[", "");
				editedSongName = editedSongName.replace("]", "");
				editedSongName = editedSongName.replace("{", "");
				editedSongName = editedSongName.replace("}", "");
				editedSongName = editedSongName.replace(":", "");
				editedSongName = editedSongName.replace("!", "");
				editedSongName = editedSongName.replace("&", "");
				editedSongName = editedSongName.replace('"', "");
				editedSongName = editedSongName.replace("out now", "");
				editedSongName = editedSongName.replace("1975", "");

				return editedSongName;
			}

			var whileLoop = 0;
			var searchCrawler = 0;
			var WhileLoopCounter = 0;
			var lastSongPlace = 0;
			var SongFromList;
			var AllSongs = "";

			//Ako je kolacic prazan napravit ce novi u kojeg ce zapisivati pjesme
			if (readCookie("FavoriteSongsList") == null){
				document.cookie = "FavoriteSongsList=; expires=Sat, 31 Dec 2016 12:00:00 UTC";
			}
			var allFavoriteSongsCookie = readCookie("FavoriteSongsList");
			var VideoTitleJavascript = <?php echo json_encode("$videoTitle"); ?>;
			

			//Petlja koja ce ucitavat pjesme iz kolacica i napravit ce listu AllSongs koja sluzi za printanje na zaslon
			while (whileLoop == 0){
				//Ako u kolacicu ne pronadje "/n/" to znaci da nema zapisanih pjesama ili da je pretrazio sve pjesme u kolacicu
				if (allFavoriteSongsCookie.indexOf("/n/", lastSongPlace) > 0) {
					WhileLoopCounter++;
					searchCrawler = allFavoriteSongsCookie.indexOf("/n/", lastSongPlace);
					SongFromList = allFavoriteSongsCookie.substring(lastSongPlace, searchCrawler);
					lastSongPlace =  lastSongPlace + SongFromList.length + 3;
					var videoTitleNewJavascript = convertForSearch(SongFromList);

					SongFromList = SongFromList.replace("(Official Video)", "");
					AllSongs = AllSongs + "<br>" + '<a class="anchor" href="search.php?q=' + videoTitleNewJavascript + '">' + SongFromList + '</a>';

					//Ako trenutno ime pjesme pronadje u listi svih pjesama to znaci da je vec odabrana kao favorit, postavit ce sliku prekrizenog srca
					if (allFavoriteSongsCookie.indexOf(VideoTitleJavascript) > -1){
						document.getElementById("FavoriteLink").style.backgroundImage = 'url("http://brorix.comxa.com/img/forbidden_icon.png")';
					}

				}else{ //Zavrsava petlju i printa sve pjesme na zaslon
					if (AllSongs == ""){ //Ako je popis pjesama prazan postavi da natpis "Favorites:" bude nevidljiv
						document.getElementById("FavoriteList").style.opacity = "0";
					}
					whileLoop = 1;
					document.getElementById("allFavorites").innerHTML = AllSongs;
				}
			}

			function add_remove_Favorite(){

				//Provjerava ima li ovu pjesmu na popisu pjesama
				if (AllSongs.indexOf(VideoTitleJavascript) > 0){ // Ako pjesma postoji brise je iz svih popisa ukljucujuci kolacice i mijenja sliku
					videoTitleNewJavascript = convertForSearch(VideoTitleJavascript);
					var SongNameToDelete = '<br><a class="anchor" href="search.php?q=' + videoTitleNewJavascript + '">' + VideoTitleJavascript + '</a>';
					AllSongs = AllSongs.replace(SongNameToDelete, "");
					document.getElementById("allFavorites").innerHTML = AllSongs;
					document.getElementById("FavoriteLink").style.backgroundImage = 'url("http://brorix.comxa.com/img/favorite_icon.png")';

					var scanCookies = readCookie("FavoriteSongsList");
					var duljinaZaObrisatiString = (VideoTitleJavascript + "/n/").length;
					var newCookie = scanCookies.replace(VideoTitleJavascript + "/n/", "");
					document.cookie = "FavoriteSongsList=" + newCookie + "; expires=Sat, 31 Dec 2016 12:00:00 UTC";

				}else{ //Ako pjesma ne postoji na popisu program je dodaje na popis kolacica i obnavlja listu pjesama na stranici, takoder mijenja sliku srca
					document.getElementById("FavoriteList").style.opacity = "1";
					var videoTitleNewJavascript = convertForSearch(VideoTitleJavascript);
					VideoTitleJavascript = VideoTitleJavascript.replace("(Official Video)", "");
					AllSongs = AllSongs + "<br>" + '<a class="anchor" href="search.php?q=' + videoTitleNewJavascript + '">' + VideoTitleJavascript + '</a>';
					document.getElementById("allFavorites").innerHTML = AllSongs;
					document.getElementById("FavoriteLink").style.backgroundImage = 'url("http://brorix.comxa.com/img/forbidden_icon.png")';
					document.cookie = "FavoriteSongsList=" + readCookie("FavoriteSongsList") + VideoTitleJavascript + "/n/" + "; expires=Sat, 31 Dec 2016 12:00:00 UTC";
				}
			}

			/* FacebookShareLinkJavascript je link kojim se dijeli web stranica na facebook-u */
			var FacebookShareLinkJavascript = "https://www.facebook.com/sharer/sharer.php?u=" + document.URL;

			/* Funkcija koja kada se klikne na gumb provjerava je li tekst sakriven, ako nije sakriva ga a ako je ne radi nista */
			var hidden = 0;
			function showLyrics(show,hide){
				if (hidden == 0){
					document.getElementById(show).className = "show";
					document.getElementById("show_hide_lyrics").innerHTML = "Hide Lyrics";
					hidden = 1;
				}else{
					document.getElementById(show).className = "hide";
					document.getElementById("show_hide_lyrics").innerHTML = "Show Lyrics";
					hidden = 0;
				}
			}

			/* Provjerava duljinu prozora i prilagodjava koliko se naslov treba pomaknuti udesno */
			var screenWidth = window.innerWidth;
			var startingVideoObject = (screenWidth - 720) / 2 - 7;
			document.getElementById("title").style.paddingLeft = startingVideoObject + "px";
			document.getElementById("ShowHideLyrics").style.left = startingVideoObject + 725 + "px";

			/* U slucaju promjene velicine prozora tekst se pomice */
			function resizedWindow(){
				var screenWidth = window.innerWidth;
				var startingVideoObject = (screenWidth - 720) / 2 - 7;
				document.getElementById("title").style.paddingLeft = startingVideoObject + "px";
			}
		</script>
		<p align="right" class="grey">Brorix is not associated with YouTube<br>Do not abuse Google products</p>
	</body>
</html>