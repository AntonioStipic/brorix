# BRORIX README #

This README contains information for this repository and explains what Brorix actually is.

### What is this repository for? ###

* This repository contains original code of Brorix website (the link is below)
* v0.2 of Brorix

### How do I get set up? ###

*Check website on link: http://www.brorix.comxa.com

### What is Brorix? ###

* Brorix is web application which you use to find your favorite songs and listen them on repeat as well as lyrics of song that can be found below video frame. Also new feature added in this version is add to favorites button which saves currently played song as favorite in cookies and writes them on the left side of the video.

### Who do I talk to? ###

* Brorix owner: Antonio Stipic (username: Spaldin)
* Email contact: antonio.stipic2@gmail.com